/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab4;
import java.util.Scanner;
/**
 *
 * @author Acer
 */
public class Lab4 {
    public static void main(String[] args) {
        boolean continueGame = false;
        do{
            XOclass game = new XOclass();
            Scanner scanner = new Scanner(System.in);

            System.out.println("Welcome to XO!");
            System.out.println("Player 1: X");
            System.out.println("Player 2: O");
            System.out.println("Let's start the game!\n");

            while (!game.checkWin() && !game.BoardFull()) {
                game.printBoard();
                int position;
                do{
                System.out.print("Player " + game.getCurrentPlayer() + ", enter your move (1-9): ");
                position = scanner.nextInt();
                } while (game.makeXO(position));

                if (game.checkWin()) {
                System.out.println("Player " + game.getCurrentPlayer() + " wins!");
                } else if (game.BoardFull()) {
                System.out.println("It's a draw!");
                }

                game.switchPlayer();
            }
            game.printBoard();
                boolean roop = false;
                do{
                    System.out.print("Do you want to continue playing? (Y/N): ");
                    String input = scanner.next();
                    char character = input.charAt(0);
                switch (character) {
                    case 'Y', 'y' -> {
                        roop = false;
                        continueGame = true;
                    }
                    case 'N', 'n' -> {
                        roop = false;
                        continueGame = false;
                    }
                    default -> roop = true;
                }
                }while(roop);
        }while(continueGame);
    }
}